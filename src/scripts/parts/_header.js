const headRectangle = document.querySelector('.header__top__menu--rectangle').childNodes;
const headRectangleLine = document.querySelector('.header__top__menu--rectangle--lines');
const headRectangleCross = document.querySelector('.header__top__menu--rectangle--cross');
const headNav = document.querySelector('.header__top__menu--nav')

headRectangle.forEach((el) => {
    el.addEventListener('click', (e) => {
      
        if(e.target.className === 'header__top__menu--rectangle--line') {
            headRectangleLine.style.display = 'none';
            headRectangleCross.style.display = 'flex';
            headNav.style.display = 'flex';
        } else if(e.target.className === 'header__top__menu--rectangle--cross') {
            headRectangleLine.style.display = 'block';
            headRectangleCross.style.display = 'none';
            headNav.style.display = 'none';
        }
    });
});